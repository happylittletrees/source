#imports for thermometer reading
import os
import glob
import time
import serial
import RPi.GPIO as GPIO
#imports for gmail reading
import imaplib
import email


heater = 17
ac = 15
fan = 26

GPIO.setmode(GPIO.BCM)
GPIO.setup(heater, GPIO.OUT)
GPIO.setup(ac, GPIO.OUT)
GPIO.setup(fan, GPIO.OUT)

def read_temp():

	tempCReceive = 50
	return tempCReceive
	#if tempCReceive != -1:
	#	temp_string = lines[1][equals_pos+2:]
	#	temp_c = float(temp_string) / 1000.0
	#	temp_f = temp_c * 9.0 / 5.0 + 32.0
	#	return temp_f

#put def for motion sensor here

def read_motion():
	motion = 1
	return motion

#put def for humidity here

def remove_email():
        #Remove used emails from mailbox
        typ, data = mail.search(None, 'ALL')
        for num in data[0].split():
                mail.store(num, '+FLAGS', '\\Deleted')
                mail.expunge()
                mail.close()
                mail.logout()


#connect to gmail

mail = imaplib.IMAP4_SSL('imap.gmail.com')
mail.login('email','password')
mail.select('inbox')
mail.list()

typ, data = mail.search(None, 'ALL')
for num in data[0].split():
	typ, data = mail.fetch(num, '(RFC822)')
typ, data = mail.search(None, 'ALL')
ids = data[0]
global id_list
id_list = ids.split()


# Any Emails?
# get most recent email id
global varSubject
if id_list:
	latest_email_id = int( id_list[-1] )
	for i in range( latest_email_id, latest_email_id-1, -1):
		typ, data = mail.fetch( i, '(RFC822)')
	for response_part in data:
		if isinstance(response_part, tuple):
			msg = email.message_from_string(response_part[1])
	varSubject = msg['subject']
	varFrom = msg['from']
	varFrom = varFrom.replace('<','')
	varFrom = varFrom.replace('>','')


# get body of the email
	bod = email.message_from_string(data[0][1])
	if bod.is_multipart():
		for payload in bod.get_payload():
			#if payload.is_multipart():...
			varBody = payload.get_payload()
	else:
		varBody = bod.get_payload()


while True:
	print("Current temp")
	print(read_temp())
	print("Target temp")
	print(varBody)

	if (varSubject == 'heat'):
		if (varBody > read_temp()):
			GPIO.output(heater, GPIO.HIGH)
                        GPIO.output(fan, GPIO.HIGH)
			print("Heater on\n")
		else:
			GPIO.output(heater, GPIO.LOW)
                        GPIO.output(fan, GPIO.LOW)
			print("Heater off\n")
	elif (varSubject == 'cool'):
		if (varBody < read_temp()):
			GPIO.output(ac, GPIO.HIGH)
			GPIO.output(fan, GPIO.HIGH)
			print("AC on\n")
		else:
			GPIO.output(ac, GPIO.LOW)
			GPIO.output(fan, GPIO.LOW)
			print("AC off\n")
	elif (varSubject == 'off'):
		GPIO.output(heater, GPIO.LOW)
		GPIO.output(ac, GPIO.LOW)
		GPIO.output(fan, GPIO.LOW)
		print("Unit off\n")
	elif (varSubject == 'auto'):
		if (read_motion() == 1):
			if (varBody > read_temp()):
				GPIO.output(heater, GPIO.HIGH)
				GPIO.output(fan, GPIO.HIGH)
				print("Motion detected - Heater on\n")
			elif (varBody < read_temp()):
				GPIO.output(ac, GPIO.HIGH)
				GPIO.output(fan, GPIO.HIGH)
				print("Motion detected - AC on\n")
			elif (varBody == read_temp()):
				GPIO.output(heater, GPIO.LOW)
				GPIO.output(ac, GPIO.LOW)
				GPIO.output(fan, GPIO.LOW)
				print("Motion detected - Unit off\n")
		if (read_motion() == 0):
			GPIO.output(heater, GPIO.LOW)
			GPIO.output(ac, GPIO.LOW)
			GPIO.output(fan, GPIO.LOW)
			print("No motion detected - Unit off\n")


	GPIO.output(fan, GPIO.HIGH)

	remove_email()
	time.sleep(30)
